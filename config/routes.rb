Rails.application.routes.draw do
  resources :users
  resources :recipes

  get 'home/index'
  get 'home/about'
  get 'home/contact'
  get 'home/privacy'
  get 'home/help'

  get 'users/create'
  get 'users/new'
  get 'users/show'

  get 'recipes/create'
  get 'recipes/new'
  get 'recipes/show'

  root 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
