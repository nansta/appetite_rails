class User < ApplicationRecord
  validates :username, length: { minimum: 6 }, presence: true
  validates :email, confirmation: true
  validates :password, length: { in: 6..20 }, confirmation: true
end
