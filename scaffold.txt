User Table
id (PK)
username
email
password
registered_on

Recipe Table
id (PK)
name
recipe_category_id (FK)

RecipeIngredient Table
id (PK)
name
amount
recipe_id (FK)

RecipeCategory Table
id (PK)
name

rails generate scaffold User username:string email:string password:string registered_on:datetime
rails generate scaffold Recipe name:string reciper_category_id:integer
rails generate scaffold RecipeIngredient name:string amount:string recipe_id:integer
rails generate scaffold RecipeCategory name:string
